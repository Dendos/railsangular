#!/bin/bash

# Install packages
yum -y remove mysql-libs
yum -y install ImageMagick sysstat
yum -y install nginx redis mysql55w mysql55w-server mysql55w-devel

# Copy configs
rm -rf /etc/nginx/conf.d/*
cp -r /home/vagrant/htdocs/conf/* /etc

# Stop services
service nginx stop
service mysqld stop
service redis stop

# Start services
service nginx start
service mysqld start
service redis start

# Auto start services
chkconfig nginx on
chkconfig mysqld on
chkconfig redis on

# Setup database
DB_USER=developer
DB_PASS=devpass
mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '${DB_USER}'@'localhost' IDENTIFIED BY '${DB_PASS}';"
mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '${DB_USER}'@'%' IDENTIFIED BY '${DB_PASS}';"
