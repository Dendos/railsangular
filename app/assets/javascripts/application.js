// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require moment
//= require moment/locale/ru

//= require faye

//= require jquery
//= require angular
//= require angular-rails-templates
//= require angular-ui-router
//= require angular-animate
//= require angular-bootstrap
//= require messageformat
//= require messageformat/locale/en
//= require messageformat/locale/ru
//= require angular-translate
//= require angular-translate-interpolation-messageformat
//= require angular-i18n/angular-locale_ru
//= require angular-xeditable
//= require AngularJS-Toaster
//= require ngstorage
//= require checklist-model
//= require ng-click-select
//= require bootstrap-sprockets

//= require Sortable
//= require Sortable/ng-sortable
//= require intl-tel-input
//= require lodash
//= require bootstrap
//= require jcrop
//= require app
//= require angular-ui-select2
//= require angular-devise

//= require_tree ./templates
//= require_tree ./controllers
//= require_tree ./models
//= require_tree ./services
