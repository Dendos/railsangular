(->
  conf = [
    'templates',
    'ng',
    'pascalprecht.translate',
    'ui.select2',
    'ui.router',
    'ui.bootstrap',
    'toaster',
    'ng-sortable',
    'ngStorage',
    'checklist-model',
    'ngClickSelect',
    'Devise',
    'xeditable'
  ]

  app = angular.module 'application', conf

  app.config ($sessionStorageProvider, $localStorageProvider, $locationProvider, $stateProvider, $provide, $urlRouterProvider, ApiProvider, AuthProvider) ->

# Enable HTML5 links
    $locationProvider.html5Mode(true)

    # States and routers
    $stateProvider

    .state 'main',
      abstract: true
      templateUrl: 'layouts/main.html'
      resolve:
        user: ['authUser', (authUser)->
          authUser.currentUser()
        ]

    .state 'main.home',
      url: '/'
      templateUrl: "layouts/main.home.html"
      controller: "HomeCtrl"
      controllerAs: "vm"

    .state 'main.test',
      url: '^/test'
      templateUrl: 'layouts/main.test.html'

    .state 'main.login',
      url: '^/login'
      templateUrl: 'layouts/main.login.html'

    #    Default page
    $urlRouterProvider.otherwise ($injector) ->
      state = $injector.get '$state'
      if (state) then state.go 'main.home'


    ApiProvider.setBase(applicationConfig.apiBase || '');

  app.run ($rootScope, $q, $state, Auth, $timeout) ->
    $rootScope.appName = 'RailsAngular'

    $rootScope.$on('devise:new-session', (e, user)->
      $rootScope.user_signed_in = true
    )

    $rootScope.$on('devise:login', (e, user)->
      $timeout ()->
        $rootScope.$broadcast('DeviseUser', user)
      3000
      $rootScope.user_signed_in = true
    )

    $rootScope.$on('devise:logout', (e, user)->
      $rootScope.$broadcast('DeviseUser', user)
      $rootScope.user_signed_in = false
    ))()
