(()=>
  AuthCtrl = ($rootScope, $scope, $state, Auth)->

    activate = ()->

    login = ()->
      Auth.login(vm.model, vm.config).then (data)->
        $rootScope.user_signed_in = true
        $state.go 'main.home'

    submitForm = (form)->
      vm.login()

    vm = this
    vm.config =
      headers:
        'X-HTTP-Method-Override': 'POST'
    vm.model = {}

    vm.submitForm = submitForm
    vm.login = login

    activate()

  angular.module('application').controller('AuthCtrl',['$rootScope','$scope', '$state', 'Auth', AuthCtrl] ))()
