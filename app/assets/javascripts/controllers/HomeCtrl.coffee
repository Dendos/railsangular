(()=>
  HomeCtrl = ($rootScope, $scope, $state, Auth, user)->
    activate = ()->

    vm = this
    vm.model = {}
    vm.config =
      headers:
          'X-HTTP-Method-Override': 'POST'
    vm.user = user._currentUser

    activate()

  angular.module('application').controller('HomeCtrl',['user','$rootScope','$scope', '$state', 'Auth', HomeCtrl] ))()
