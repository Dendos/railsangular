TestModel = (Api)->
  {
    list: (data) ->
      Api.get('test', data)
  }

angular.module('application').factory('TestModel', TestModel)

