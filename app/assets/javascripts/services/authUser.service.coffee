do ->
  'use strict'
  angular.module('application').factory 'authUser', ($rootScope, Auth) ->
    {
    currentUser: ->
      resolve = (user)->
        return user
      error = ()->
        return {}

      Auth.currentUser().then resolve, error

    getUser: ->
      Auth.currentUser()
    }
