(function() {
    'use strict';

    angular.module('application').provider('Api', function() {

        var apiBase = '';
        return {
            setBase: function(base) {
                apiBase = base;
            },
            $get: function($rootScope, $log, $http) {
                return {
                    $makeCall: function(method, url, data) {
                        var request = {
                            method: method,
                            url: apiBase + url
                        };
                        if ('GET' === method || 'DELETE' === method) {
                            request.params = data;
                        } else {
                            request.data = data;
                        }
                        if (data instanceof FormData) {
                            request.transformRequest = angular.identity;
                            request.headers['Content-Type'] = undefined;
                        }
                        return $http(request).then(function(response) {
                            if (angular.isObject(response.data)) {
                                return _.merge({status: 'success'}, response.data);
                            } else {
                                return {
                                    status: 'error',
                                    message: 'error.msg_unavailable_service'
                                };
                            }
                        }, function(response) {
                            if (angular.isObject(response.data)) {
                                return _.merge({statusCode: response.status, status: response.status >= 500 ? 'error' : 'warning'}, response.data);
                            } else {
                                return {
                                    status: 'error',
                                    message: 'error.msg_unavailable_service'
                                };
                            }
                        });
                    },

                    post: function(url, data) {
                        return this.$makeCall('POST', url, data);
                    },

                    get: function(url, data) {
                        return this.$makeCall('GET', url, data);
                    },

                    put: function(url, data) {
                        return this.$makeCall('PUT', url, data);
                    },

                    patch: function(url, data) {
                        return this.$makeCall('PATCH', url, data);
                    },

                    delete: function(url, data) {
                        return this.$makeCall('DELETE', url, data);
                    }
                };
            }
        };
    });

})();
