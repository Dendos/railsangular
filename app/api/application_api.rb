class ApplicationApi < Grape::API
  version 'v1', using: :header, vendor: 'aptos'
  format :json

  mount TestApi
end
