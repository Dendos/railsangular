class TestApi < Grape::API
  resource :test do
    params do
      optional :search, type: String
    end
    desc 'Logs list'
    get do
      [
          {
              id: 'name',
              name: params[:search]
          },
          {
              id: 'test',
              name: 'params[:search]'
          }
      ]
    end
  end

end
