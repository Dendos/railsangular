source 'https://rubygems.org'

gem 'rails', '4.2.4'
gem 'mysql2', '~> 0.3.20'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'jbuilder', '~> 2.0'
gem 'bcrypt', '~> 3.1.7'
gem 'pundit'
gem 'figaro'
gem 'enumerize'
gem 'email_validator'
gem 'acts_as_paranoid', :git => 'https://github.com/ActsAsParanoid/acts_as_paranoid.git'
gem 'rails-i18n', '~> 4.0.0'
gem 'sprockets', '2.12.3'
gem 'rails-settings-cached', '~> 0.4.0'
gem 'database_cleaner', '~> 1.5.0'
gem 'debase'
gem 'ruby-debug-ide'
gem 'paperclip', '~> 4.3'
gem 'devise'
gem "zeus", ">= 0.13.4.pre2"

# Country list
gem 'countries', require: 'countries/global'

# API
gem 'grape'
gem 'grape-entity'
gem 'grape-rails-routes'

# WebSocket API
gem 'thin'
gem 'faye-rails'
gem 'faye-redis', platforms: :ruby

# Assets
gem 'bower-rails'
gem 'angular-rails-templates'
gem 'ngannotate-rails'
gem 'font-awesome-rails'
gem 'flag_icon_css_rails'
gem 'bootstrap-sass'
gem 'angular-ui-select2-rails'
gem 'coffee-rails'

# Cron jobs
gem 'whenever'

source "https://rails-assets.org" do
  gem "rails-assets-angular-devise"
end

platforms :ruby do
  gem 'therubyracer'
end

group :development, :test do
  gem 'pry-rails'
  gem 'quiet_assets'
  gem 'rspec-rails', '~> 3.0'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'tzinfo-data', platforms: [:mingw, :mswin]
end

group :development do
  gem 'web-console', '~> 2.0'
  gem 'spring', '= 1.3.6'
end
