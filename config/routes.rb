Rails.application.routes.draw do
  devise_for :users
  root 'pages#home'

  mount ApplicationApi => '/api'

  get :reset, to: 'pages#home', as: :reset
  get :activate, to: 'pages#home', as: :activate

  match '*path', to: 'pages#home', via: :all
end
